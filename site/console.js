if (!console_container) {
  console.error('Expected a #console_container element')
}
if (!console_element) {
  console.error('Expected a #console_element element')
}

const createCursor = () => {
  const span = document.createElement('span')
  span.classList.add('cursor')
  return span
}

const scrollToBottom = () => {
  const { clientHeight, scrollHeight, scrollTop } = console_container
  const topWhenScrolledToBottom = scrollHeight - clientHeight
  console_container.scrollTop = topWhenScrolledToBottom
}

let isScrollInProgress = false
const maintainScroll = () => {
  if (isScrollInProgress) {
    return
  }
  isScrollInProgress = true

  const { clientHeight, scrollHeight, scrollTop } = console_container
  const topWhenScrolledToBottom = scrollHeight - clientHeight
  const scrollSnapHeight = 50 // snap to bottom if within this many pixels

  const wasScrolled = Math.abs(topWhenScrolledToBottom - scrollTop) < scrollSnapHeight
  if (!wasScrolled) {
    isScrollInProgress = false
    return
  }

  setTimeout(() => {
    scrollToBottom()
    isScrollInProgress = false
  })
}

let readKeyCallback
window.readKey = (callback) => {
  if (readKeyCallback) {
    throw new Error('Already registered')
  }
  readKeyCallback = callback
  console_element.append(createCursor())
}

window.write = (node) => writeInternal([node])
window.writeln = (node) => writeInternal([node, document.createElement('br')])

const writeInternal = (nodes) => {
  maintainScroll()

  for (const cursor of console_element.querySelectorAll('.cursor')) {
    cursor.remove()
  }

  for (const node of nodes.filter(Boolean)) {
    console_element.append(node)
  }

  if (readKeyCallback) {
    console_element.append(createCursor())
  }
}

scrollToBottom()

console_container.addEventListener('click', () => {
  console_element.focus()
})

console_element.addEventListener('keydown', (evt) => {
  if (!readKeyCallback || evt.key.length > 1) {
    return
  }

  evt.stopPropagation()
  for (const cursor of console_element.querySelectorAll('.cursor')) {
    cursor.remove()
  }

  const currentCallback = readKeyCallback
  readKeyCallback = null
  currentCallback(evt.key)
  scrollToBottom()
})
