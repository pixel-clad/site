const autofocusedEl = document.querySelector('[autofocus]')
if (autofocusedEl) {
  autofocusedEl.focus()
}

const focusableSelector = 'a, input, select, textarea, [tabindex]'
const getFocusable = () => Array.from(document.querySelectorAll(focusableSelector))
  .filter((el) => el.getAttribute('tabindex') >= 0)

const getNext = () => {
  const focusable = getFocusable()
  if (focusable.length === 0) {
    return { focus: () => { } }
  }
  const i = focusable.indexOf(document.activeElement)
  return focusable[(i < 0) ? 0 : (i + 1) % focusable.length]
}

const getPrev = () => {
  const focusable = getFocusable()
  if (focusable.length === 0) {
    return { focus: () => { } }
  }
  const i = focusable.indexOf(document.activeElement)
  if (i < 0) {
    return focusable[0]
  } else if (i === 0) {
    return focusable[focusable.length - 1]
  } else {
    return focusable[i - 1]
  }
}

document.addEventListener('keydown', (evt) => {
  const nextKeys = ['ArrowDown', 'Down', 'j', 'J']
  const prevKeys = ['ArrowUp', 'Up', 'k', 'K']

  if (nextKeys.includes(evt.key)) {
    evt.preventDefault()
    const next = getNext()
    next && next.focus()
  } else if (prevKeys.includes(evt.key)) {
    evt.preventDefault()
    const prev = getPrev()
    prev && prev.focus()
  }
})
